const superHeroes = [
  {
    nombre: "ironman",
    likes: 88,
    dislikes: 12,
    img: "assets/img/iron.jpg",
    imgthumb: "assets/img/iron_thumb.jpg",
  },
  {
    nombre: "Superman",
    likes: 32,
    dislikes: 64,
    img: "assets/img/super.jpg",
    imgthumb: "assets/img/super_thumb.jpg",
  },
  {
    nombre: "batman",
    likes: 64,
    dislikes: 37,
    img: "assets/img/bat.jpg",
    imgthumb: "assets/img/bat_thumb.jpg",
  },
  {
    nombre: "thor",
    likes: 64,
    dislikes: 37,
    img: "assets/img/thor.jpg",
    imgthumb: "assets/img/thor_thumb.jpg",
  },
  {
    nombre: "spiderman",
    likes: 64,
    dislikes: 37,
    img: "assets/img/spider.jpg",
    imgthumb: "assets/img/spider_thumb.jpg",
  },
];

Vue.component("box-desc", {
  props: ["nombre", "likes", "dislikes", "index", "img"],
  template: `<div class="card_infoHeroe">
                    <div class="contenedorInfohero_card" v-bind:style="{background: 'url('+img+')' +'center / cover' }" >
                        <div class="mdl-grid  posicion_card">
                            
                            <div class="mdl-cell mdl-cell--12-col" >

                                <div class="iconvoto_card" v-bind:class="{positivo : mayor(likes,dislikes), 'negativo': !mayor(likes,dislikes)}"  >
                                    <span  class="material-icons " v-if="mayor(likes,dislikes)===true"> thumb_up </span>
                                    <span  class="material-icons " v-else> thumb_down </span>
                                </div>

                                <div class="font_regular titulo_infoHero color_blanco" style="margin-left: 30px;" >{{nombre}}</div>
                            </div>
                            <div class="mdl-cell mdl-cell--12-col  color_blanco">
                                <p class="font_light">Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                                    Lorem Ipsum</p>
                            </div>

                            <div class="mdl-cell mdl-cell--12-col  color_blanco detailinfohero">
                                    <button v-on:click="detalle(index)" class="boton-border">detalles</button>
                                <div style="text-align: end;line-height: 14px;">
                                    <p class="font_bold">hace 1 mes <br>  en lorem dolor</p>
                                </div>  
                        </div>
                        </div>
                    <div class="contentinfoHero">
                        <content-votacioncard v-bind:likes=likes v-bind:dislikes=dislikes ></content-votacion>
                    </div>
                    </div>
                </div>`,
  methods: {
    mayor: function (likes, dislikes) {
      return likes > dislikes ? true : false;
    },
    detalle: function (index) {
      app.$emit("detalle", index);
    },
  },
});

Vue.component("content-votacion", {
  props: ["likes", "dislikes"],
  template: `<div  class="contenedorVoto">
                    <div class="contentlike" v-bind:style="{width:  calculolike(likes,dislikes)+'%' }" >
                        <span class="material-icons font_iconHeaderinfo"> thumb_up </span>
                        <div class="font_light " style="font-size: 35px;">  {{ calculolike(likes,dislikes)}}%</div>
                    </div>
                    <div class="contentdislike" v-bind:style="{width:  calculodislike(likes,dislikes)+'%' }" >
                        <div class="font_light" style="font-size: 35px;">{{calculodislike(likes,dislikes)}}%</div>
                        <span class="material-icons font_iconHeaderinfo"> thumb_down </span>
                        
                    </div>
                </div>`,
  methods: {
    calculolike: (likes, dislikes) => {
      return Math.round((likes / (likes + dislikes)) * 100);
    },
    calculodislike: (likes, dislikes) => {
      return Math.round((dislikes / (likes + dislikes)) * 100);
    },
  },
});

Vue.component("content-votacioncard", {
  props: ["likes", "dislikes"],
  template: `<div  class="contenedorVotocard">
                    <div class="contentlike_card" v-bind:style="{width:  calculolike(likes,dislikes)+'%' }" >
                        <span class="material-icons "> thumb_up </span>
                        <div class="font_light " style="font-size: 15px;">  {{ calculolike(likes,dislikes)}}%</div>
                    </div>
                    <div class="contentdislike_card" v-bind:style="{width:  calculodislike(likes,dislikes)+'%' }" >
                        <div class="font_light" style="font-size: 15px;">{{calculodislike(likes,dislikes)}}%</div>
                        <span class="material-icons "> thumb_down </span>
                        
                    </div>
                </div>`,
  methods: {
    calculolike: (likes, dislikes) => {
      return Math.round((likes / (likes + dislikes)) * 100);
    },
    calculodislike: (likes, dislikes) => {
      return Math.round((dislikes / (likes + dislikes)) * 100);
    },
  },
});

var app = new Vue({
  el: "#contenedor",
  data: {
    lista: superHeroes,
    cambio: true,
    tipoVoto: false,
    heroeSelect: {
      index: 0,
      nombre: "",
      likes: 0,
      dislikes: 0,
      imgPath: "",
    },
  },
  methods: {
    like: function (index) {
      this.cambio = false;
      this.tipoVoto = true;
      this.heroeSelect.likes += 1;
      this.lista[index].likes += 1;
    },
    dislike: function (index) {
      this.heroeSelect.dislikes += 1;
      this.lista[index].dislikes += 1;
      this.tipoVoto = false;
      this.cambio = false;
    },
    votar: function () {
      this.cambio = true;
    },
    seleccion: function (index) {
      this.heroeSelect.nombre = superHeroes[index].nombre;
      this.heroeSelect.likes = superHeroes[index].likes;
      this.heroeSelect.dislikes = superHeroes[index].dislikes;
      this.heroeSelect.imgPath = superHeroes[index].img;
      this.heroeSelect.index = index;
      window.scrollTo(0, 0);
    },
  },
  mounted() {
    this.heroeSelect.nombre = superHeroes[0].nombre;
    this.heroeSelect.likes = superHeroes[0].likes;
    this.heroeSelect.dislikes = superHeroes[0].dislikes;
    this.heroeSelect.imgPath = superHeroes[0].img;

    this.$on("detalle", function (index) {
      this.seleccion(index);
    });
  },
});
